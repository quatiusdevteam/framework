<?php

namespace Quatius\Framework\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Quatius\Framework\Traits\CacheableUpdate;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Helpers\CacheKeys;

abstract class QuatiusRepository extends BaseRepository implements CacheableInterface {
    
    use CacheableRepository, CacheableUpdate;
    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    private function _getPublished($publishOnly=true){
        
        if ($publishOnly){
            return $this->parserResult($this->makeModel()->query()->published()->get());
        }
        else {
            return $this->parserResult($this->makeModel()->query()->get());
        }
    }

    /**
     * Get Cache key for the method
     *
     * @param $method
     * @param $args
     *
     * @return string
     */
    public function getCacheKey($method, $args = null)
    {
        $args = serialize($args);
       
        $baseUrl = config("quatius.framework.cache.url-filter-enabled", true)?url("/"):"";

        $key = sprintf('%s@%s-%s', get_called_class(), $method, md5($args.$baseUrl));

        CacheKeys::putKey(get_called_class(), $key);

        return $key;

    }
    
    public function getPublished($publishOnly=true){
        if (!$this->allowedCache('getPublished') || $this->isSkippedCache()) {
            return $this->_getPublished();
        }
        
        $key = $this->getCacheKey('getPublished', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use($publishOnly){
            return $this->_getPublished($publishOnly);
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
}
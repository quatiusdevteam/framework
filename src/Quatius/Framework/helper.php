<?php
 /**
 *	Framework Helper  
 */

if (!function_exists('theme_asset')) {
    if (!function_exists('theme_asset')) {
        /**
         * Get translated url.
         *
         * @param string $url
         *
         * @return string
         */
        function theme_asset($file)
        {
            return app('theme')->asset()->url($file);
        }
    }
}


if (!function_exists('user_check'))
{
    function user_check(){
        
        return !!user()->id;
    }
}

if (!function_exists('users'))
{
    function users($field=""){
        
        return user()->$field;
    }
}

if (!function_exists('get_guard'))
{
    function get_guard($url=""){
        return Trans::to(config('auth.guards.'.config("auth.defaults.guard").'.url', config("auth.defaults.guard") == "admin.web"?"admin":"")."/".$url);
    }
}

if (!function_exists('eventFire'))
{
	function eventFire($event, $params){
		if (5.8 <= floatval(app()->version()))
            Event::dispatch($event, $params);
        else
            Event::fire($event, $params);
    }
}

if (!function_exists('requestFile')) {
	function requestFile($request, $storage = 'local', $filename=null, $dir=''){
		$files = array_values($request->allFiles());
		$file = isset($files[0])?$files[0]:null;

        if ($file && $file->isValid())
		{
			$filename = $filename?:$file->getClientOriginalName();

            $path = $dir.$filename;
			$savePath = storage_path('app/'.$path);
		 
			syncStorageFile($path, $storage, $file);

			return $savePath;
		}
		return null;
	}
}

if (!function_exists('syncStorageFile'))
{
	function syncStorageFile($path, $storage = 'local', $putfile = null){
		$useCloud = $storage != 'local';

		if ($putfile !== null){
			if (is_string($putfile) && File::isFile($putfile))
				$putfile = new Illuminate\Http\File($putfile);
			
			if ($putfile instanceof \Illuminate\Http\UploadedFile){
				$putfile->storeAs(dirname($path), basename($path));
				$putfile = new Illuminate\Http\File(storage_path('app/'.$path));
			}

			if ($useCloud && $putfile instanceof \Illuminate\Http\File){
				Storage::disk($storage)->putFileAs(dirname($path), $putfile, basename($path), 'private');
				$lastMod = Storage::disk($storage)->lastModified($path);
				touch($putfile->getRealPath(), $lastMod);
				return $path;
			}
			return "";
		}

		if (!$useCloud){
			if (Storage::disk('local')->exists($path)) return $path;
			return "";
		}

		if (Storage::disk('local')->exists($path) && Storage::disk($storage)->exists($path)){
			$localMod = Storage::disk('local')->lastModified($path);
			$cloudMod = Storage::disk($storage)->lastModified($path);

			if ($localMod == $cloudMod){
				return $path;
			}
			else{
				$contents =  Storage::disk($storage)->get($path);
				storage::disk('local')->put($path, $contents);
				touch(storage_path('app/'.$path), $cloudMod);
				return $path;
			}
		}
		elseif (!Storage::disk('local')->exists($path) && Storage::disk($storage)->exists($path)) {
			$cloudMod = Storage::disk($storage)->lastModified($path);
			$contents =  Storage::disk($storage)->get($path);
			storage::disk('local')->put($path, $contents);
			touch(storage_path('app/'.$path), $cloudMod);
			return $path;
		}
		return "";
	}
}

if (!function_exists('hasModelChanged'))
{
	function hasModelChanged($model, $datas){

        if($datas){
            foreach ($datas as $field=>$value){
                if (!isset($model->$field)) return true;

                if ($model->$field != $value){
                    return true;
                }
            }
        }

        return false;
	}
}

if (!function_exists('removeExpiredCache'))
{
    function removeExpiredCache()
    {
        if (config('cache.default', 'file') != 'file' || 
            // clean interval check 14400 sec or 4 hours
            !(time() > config('quatius.framework.cache.clean-interval', 14400) + setting('system.cache','last-clean', 0))) {
            return;
        }

        \Config::set('filesystems.disks.fcache', [
            'driver' => 'local',
            'root'   =>  storage_path('framework/cache')
        ]);

        setting_save('system.cache','last-clean', time());

        $storage = app('filesystem');
        // grab the cache files
        $d = $storage->disk('fcache')->allFiles();
    
        $sizeCleaned = 0;
        // loop the files
        foreach ($d as $key => $cachefile) {
    
            // ignore that file
            if ($cachefile == '.gitignore') {
                continue;
            }
    
            // grab the contents of a file
            $d1 = $storage->disk('fcache')->get($cachefile);
    
            // grab the expire time from the file
            $expire = substr($contents = $d1, 0, 10);
    
            // check if it has expired
            if (time() >= $expire) {
                // delete the cachefile
                $sizeCleaned += $storage->disk('fcache')->size($cachefile);
                $storage->disk('fcache')->delete($cachefile);
            }
        }

        //app('log')->info("Cache Cleaned: $sizeCleaned");
    }
}

if (!function_exists('dataTableQuery'))
{
    function dataTableQuery($postData, $query, $autoTransform=true) // $autoTransform \Presenter\FractalPresenter
    {
        $columns = array_get($postData, 'columns', []);
        $limit = array_get($postData, 'length', 10);
        $start = array_get($postData, 'start', 0);
        $firstOrderIndex = intval(array_get($postData, 'order.0.column', "0"));

        $order = isset($columns[$firstOrderIndex]["data"])?$columns[$firstOrderIndex]["data"]:null;
        $dir = array_get($postData, 'order.0.dir', "desc");
        $totalData = $order !== null?$query->count($order):$query->count();
        $totalFiltered = $totalData; 

        $search = trim(array_get($postData, 'search.value', ""));

        if(!empty($search))
        {
            $query->where(function($query) use ($columns, $search){
                $isFirstCont = true;
                foreach($columns as $column){
                    if($column["searchable"] != "true") continue;
    
                    if ($isFirstCont)
                        $query->where($column['data'],'LIKE',"%{$search}%");
                    else 
                        $query->orWhere($column['data'], 'LIKE',"%{$search}%");
                    
                    $isFirstCont = false;
                }
            });

            $totalFiltered = $query->count();
        }

        /** select **/
        $selectFields = [];
        foreach($columns as $column){
            $selectFields[] = $column['data'];
        }

        foreach(array_get($postData, 'extra_fields', []) as $extra_field){
            $selectFields[] = $extra_field;
        }

        $hashIds = array_get($postData, 'hashids', []);
        if (array_get($postData, 'hash.from', '')){
            $hashIds[array_get($postData, 'hash.from', '')] = array_get($postData, 'hash.to', 'eid');
        }

        if ($selectFields){
            $query->select(DB::raw(implode(',', $selectFields)));
            if(!$hashIds){
                foreach ($hashIds as $from=>$to)
                    $selectFields[] = $from;
            }
        }

        if ($limit > 0)
            $query->offset($start)->limit($limit);

        if ($order !== null)
            $query->orderBy($order,$dir);

        $results = $query->get();
        $data = [];
        
        if (!is_array($results)){
            if ($autoTransform && $results instanceof \Illuminate\Database\Eloquent\Collection){
                if ($autoTransform instanceof \Prettus\Repository\Presenter\FractalPresenter){
                    $dataParse = $autoTransform->present($results);
                    
                    $data = isset($dataParse["data"])?$dataParse["data"]:$dataParse;
                    
                }else 
                if ($results->first() instanceof \Prettus\Repository\Contracts\Transformable){
                    foreach ($results as $result)
                        $data [] =  $result->transform();
                }
                else
                    $data = $results->toArray();
            }
            else
                $data = $results->toArray();
        }
        else
            $data = $results;
		
        for($i= 0 ; $i < count($data); $i++){
            $data[$i] = array_map(function($val){
                return htmlspecialchars($val);
            }, (array) $data[$i]);

            foreach ($hashIds as $from=>$to){
                if(!empty($from) && isset($data[$i][$from])){
                    $data[$i][$to] = hashids_encode($data[$i][$from]);
                }
            }
        }

        $json_data = array(
            "draw"            => intval(array_get($postData, 'draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data   
        );

        return $json_data;

        /* javascript example
            oTable = $('#main-list').DataTable({
            "columns": [
                { "data": "updated_at" },
                { "data": "id" },
                { "data": "name" },
                { "data": "email" }
            ],
            "order": [[ 0, "desc" ]],
            serverSide: true,
            ajax: {
                url: "{{ URL::to('/admin/user/user') }}",
                "dataType": "json",
                "type": "GET",
                "data":{
                // _token: "{{csrf_token()}}",
                    extra_fields : [
                        "username","status"
                    ],
                    hash:{
                        from: "id",
                        to: "eid"
                    }
                }
            }
        */
    }
}

if (!function_exists('array_tree'))
{
    function array_tree($srcfolders, &$root=[], $folderSeperator="/", $dotReplace="&#46;"){
        if (is_array($srcfolders)){
            sort($srcfolders);
            foreach ($srcfolders as $folder){
                array_tree($folder, $root);
            }
        }elseif(is_string($srcfolders)){
            $folderFilter = str_replace(".",$dotReplace, $srcfolders);
            $folderFilter = str_replace($folderSeperator,".", $folderFilter);
            array_set($root,$folderFilter, []);
        }
		return $root;
    }
}

if (!function_exists('updateMorphClass'))
{
    function updateMorphClass($table, $id , $from , $to, $morphField="")
    {
        $morphField = empty($morphField)?str_singular($table):$morphField;
        
        return \DB::table($table)
            ->where($morphField.'_id',$id)
            ->where($morphField.'_type',$from)
            ->update([$morphField.'_type'=>$to]);
    }
}

if (!function_exists('renderBlade')){
    function renderBlade($bladeString, $__data=[])
{
    $php = Blade::compileString($bladeString);
    $obLevel = ob_get_level();
    ob_start();
    extract($__data, EXTR_SKIP);
    try {
        eval('?' . '>' . $php);
    } catch (Exception $e) {
        while (ob_get_level() > $obLevel) ob_end_clean();
        throw $e;
    } catch (Throwable $e) {
        while (ob_get_level() > $obLevel) ob_end_clean();
        throw new \Symfony\Component\Debug\Exception\FatalThrowableError($e);
    }
    return ob_get_clean();
}
}
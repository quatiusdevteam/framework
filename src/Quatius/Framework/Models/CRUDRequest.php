<?php

namespace Quatius\Framework\Models;

use App\Http\Requests\Request;

class CRUDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(\Illuminate\Http\Request $request)
    {
        $user = $request->user();
        $modules = request()->route()->getAction()['module'];
        $permission_slug = is_array($modules)? $modules[count($modules)-1]: $modules;
        $permission_slug = strtolower($permission_slug);
        
        if ($request->isMethod('POST') || $request->is('*/create')) {
            // Determine if the user is authorized to create an entry.
            return $user->canDo($permission_slug.'.create');
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            // Determine if the user is authorized to update an entry.
            return $user->canDo($permission_slug.'.edit');
        }

        if ($request->isMethod('DELETE')) {
            // Determine if the user is authorized to delete an entry.
            return $user->canDo($permission_slug.'.delete');
        }
        // Determine if the user is authorized to view the module.
        return $user->canDo($permission_slug.'.view');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    // public function rules(\Illuminate\Http\Request $request)
    // {
    //     $user = $this->route('user');

        // if ($request->isMethod('POST')) {
        //     // validation rule for create request.
        //     return [
        //         'name'     => 'required|max:255',
        //         'email'    => 'required|email|max:255|unique:hdms_users',
        //         'password' => 'required|min:6',
        //     ];
        // }

        // if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
        //     // Validation rule for update request.
        //     return [
        //         'name'  => 'required|max:255',
        //         'email' => 'required|email|max:255|unique:hdms_users,email,' . $user->id,
        //     ];
        // }

        // Default validation rule.
    //     return [

    //     ];
    // }
}

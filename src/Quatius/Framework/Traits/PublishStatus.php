<?php

namespace Quatius\Framework\Traits;

/**
 * @author seyla.keth
 *
 */
trait PublishStatus
{
	public function scopePublished($query){
		$query->whereStatus(2);
		
		return $query;
	}
	
	public function scopeUnpublish($query){
	    $query->whereStatus(1);
	    
	    return $query;
	}
	
	public function scopeStatus($query, $status){
	    if ($status !== null)
	    {
	        if (is_array($status)){
	            $query->whereIn('status',$status);
	        }
	        else{
	            $status = intval($status);
	            $query->where('status',$status);
	        }
	    }
	    return $query;
	}
}

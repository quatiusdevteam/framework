<?php

namespace Quatius\Framework\Traits;
use DB;
use Quatius\Framework\Models\ModelItemGroup;

trait HasModelGrouping
{	
	public static function bootHasModelGrouping()
	{
		static::deleting(function($item){
			$item->modelGroups()->delete();
		});
	}
	
	public static function getGroupQuery($typeClass, $strGroupIdenitifer= null, $id = null, $data = null)
	{
	    $groupQuery = ModelItemGroup::whereGrouptableType($typeClass);
	    if ($strGroupIdenitifer){
	        $groupQuery->whereGroupName($strGroupIdenitifer);
	    }
	    if ($id){
	        $groupQuery->whereGrouptableId($id);
	    }
	    if ($data){
	        $groupQuery->whereGroupData($data);
	    }
	    return $groupQuery;
	}
	
	public function modelGroups()
	{
		return $this->morphMany(ModelItemGroup::class, 'grouptable');
	}
	
	public function removeItemGroup($strGroupIdenitifer){
	    return $this->modelGroups()->whereGroupName($strGroupIdenitifer)->delete();
	}
	
	public function getItemGroup($strGroupIdenitifer){
	    return $this->modelGroups()->whereGroupName($strGroupIdenitifer)->get();
	}
	
	public function addItemGroup($strGroupIdenitifer, $strData = "", $allowMulitple = false)
	{
	    $groupitem = $this->getItemGroup($strGroupIdenitifer)->first();
		
	    if ($groupitem && !$allowMulitple)
		{
			ModelItemGroup::where('group_name', $groupitem->group_name)
				->where('grouptable_type', $groupitem->grouptable_type)
				->where('grouptable_id', $groupitem->grouptable_id)
				->update(["group_data"=>$strData]);
		}
		else {
			$groupitem = new ModelItemGroup();
			$groupitem->group_name = $strGroupIdenitifer;
			$groupitem->group_data = $strData;
			$this->modelGroups()->save($groupitem);
		}
	}
	
	public function getItemGroupData($strGroupIdenitifer, $toJson = false, $toArray = true)
	{
	    $groupitem = $this->getItemGroup($strGroupIdenitifer)->first();
	
		if ($groupitem)
		{
			if ($toJson)
				return json_decode($groupitem->group_data, $toArray);
			
			return $groupitem->group_data;
		}
		else {
			if ($toJson)
				return $toArray?[]:json_decode("{}");
		}
		return "";
	}
	
	public function ScopeWithGroup($query)
	{
		$query->leftJoin(DB::raw('(select group_name, group_data, grouptable_id FROM model_item_groups) modelitemtrait'), 
			'id', '=', 'grouptable_id');
		
		return $query;
	}
	
	public function ScopeItemData($query, $data)
	{
		$query->leftJoin(DB::raw('(select group_name, group_data, grouptable_id FROM model_item_groups) modelitemtrait'), 
			'id', '=', 'grouptable_id');
		
		$query->whereGroupData($data);
		
		return $query;
	}
	
	public function ScopeItemGroup($query, $strGroupIdenitifer, $data=null)
	{
		$query->leftJoin(DB::raw('(select group_name, group_data, grouptable_id FROM model_item_groups) modelitemtrait'),
				'id', '=', 'grouptable_id');
		
		$query->whereGroupName($strGroupIdenitifer);
		
		if ($data)
			$query->whereGroupData($data);
		
		return $query;
	}
}

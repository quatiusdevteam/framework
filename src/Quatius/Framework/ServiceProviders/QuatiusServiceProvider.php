<?php

namespace Quatius\Framework\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;
use Blade;
use Route;
use Event;

/**
 * @author seyla.keth
 *
 */
class QuatiusServiceProvider extends ServiceProvider
{
	protected $files = true;
	protected $dirModules = [];
	protected $disableModules = [];
	
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
	    $this->publishes([
	        realpath(__DIR__.'/../').'/Publishes/public-forced'=>public_path()
	    ], 'public');
	    
	    Blade::directive('position', function($placement)
	    {
	        $placement = trim(str_replace(["'",'"','(',')'], '', $placement));
	        return "<?php echo Theme::place('".$placement."'); ?>";
	    });
	    
	    Blade::directive('includeStyle', function($assetPath)
	    {
	        $assetPath = str_replace(["'",'"','(',')'], '', $assetPath);
	        return "<?php Theme::asset()->add('".basename($assetPath)."', '".$assetPath."'); ?>";
	    });
	    
	    Blade::directive('includeScript', function($assetPath)
	    {
	        $assetPath = str_replace(["'",'"','(',')'], '', $assetPath);
	        return "<?php Theme::asset()->add('".basename($assetPath)."', '".$assetPath."'); ?>";
	    });
	    
	    // Add @Script
	    Blade::directive('script', function()
	    {
	        return '<?php ob_start(); ?>';
	    });
	    
	    // Add @endScript
	    Blade::directive('endscript', function()
	    {
	        $this->scriptCount = isset($this->scriptCount)?$this->scriptCount + 1: 1;
	        //check if $scriptContent is empty;
	        return '<?php  $scriptContent = trim(ob_get_contents()); ob_end_clean(); if ($scriptContent!="") Theme::asset()->writeContent("'.$this->scriptCount.'"."_".basename(__FILE__)."-inline-script", $scriptContent);  ?>';
	    });
	    
		$this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
	    
		$this->publishes([
			realpath(__DIR__.'/../config_tmpl.php') => config_path('quatius/framework.php')
		]);
			
		foreach($this->dirModules as $modulepath)  {
			$module = class_basename($modulepath);
			$moduleCfg = strtolower($module);
		
			if (array_search($module, $this->disableModules) !== false)
				continue;
			
			if (!$this->app->routesAreCached() && config("quatius.framework.include-route.".$moduleCfg, true)) {
				$routes = $modulepath.'/routes.php';
				if($this->files->exists($routes)) include $routes;
			}
			
			// Setup app views
			$app_views = app_path('Modules/'.$module.'/Views');
			if ($this->files->exists($app_views)){
			    $this->loadViewsFrom($app_views, $module);
			}
			
			// Setup vendor views
			$views  = $modulepath.'/Views';
			if($this->files->isDirectory($views) && config("quatius.framework.include-view.".$moduleCfg, true)){
			    $this->loadViewsFrom($views, 'Quatius'.$module);//prefix alternate
			    $this->loadViewsFrom($views, $module);
			}
			
			// Setup translations
			$app_trans = app_path('Modules/'.$module.'/Translations');
			if($this->files->isDirectory($app_trans)){
			    $this->loadTranslationsFrom($app_trans, $module);
			}
			
			$trans  = $modulepath.'/Translations';
			if($this->files->isDirectory($trans) && config("quatius.framework.include-trans.".$moduleCfg, true)){
			    $this->loadTranslationsFrom($trans, $module);
			}
			
			// Setup Config disable config as it will be merge
			/* $config = $modulepath.'/config.php';
			if($this->files->exists($config) )
			{
			    $config_tmpl = realpath(__DIR__.'/../config_tmpl.php');
			    $this->publishes([
			        $config_tmpl => config_path('quatius/framework.php')
				]);
			} */
			
			$migrations = $modulepath.'/Database/migrations';
			if($this->files->isDirectory($migrations) && config("quatius.framework.include-migrate.".$moduleCfg, true))
			{
				$migrationFiles = $this->files->files($migrations);
				
				$migrationFilesDes = array_map(function ($path){
					return database_path('migrations/'.class_basename($path));
				},$migrationFiles);
				
				$this->publishes(array_combine($migrationFiles, $migrationFilesDes));
			}
		}
		// Laravel 5.3 does not fire this event
		Route::matched(function($params) {
			$route = $params->route;
			$request = $params->request;
			eventFire('router.matched', [$route, $request]);
		});
	}
	
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		config()->set('theme.events.before',function($theme)
		{
			eventFire('theme.events.before', [$theme]);
		});

		config()->set('theme.events.after',function($theme)
		{
			eventFire('theme.events.after', [$theme]);
        });

		$this->files = new Filesystem();
		$this->quatius_path = realpath(__DIR__.'/../../../../../');
		
		if(is_dir($this->quatius_path)) {
			
			if(!$this->files->isDirectory(config_path().'/quatius'))
				$this->files->makeDirectory(config_path().'/quatius');
			
			$mainModules = $this->files->directories($this->quatius_path);
			
			$this->dirModules = [];
			
			foreach($mainModules as $mainModule)  {
				if(is_dir($mainModule.'/src/Quatius')) {
					$modules = $this->files->directories($mainModule.'/src/Quatius');
					$this->dirModules = array_merge($this->dirModules,$modules);
				}
			}
			
			$this->disableModules = config("quatius.framework.disable") ?: [];
			
			foreach($this->dirModules as $modulepath)  {
				$module = class_basename($modulepath);
				
				if (array_search($module, $this->disableModules) !== false)
					continue;
				
				$config = $modulepath.'/config.php';
				
				if($this->files->exists($config) )
				{
					$this->mergeConfigFrom(
						$config, 'quatius.'.strtolower($module)
					);
				}
				
				$helper = $modulepath.'/helper.php';
				
				if($this->files->exists($helper)) include $helper;
				
				$providers  = $modulepath.'/Providers';
				if($this->files->isDirectory($providers))
				{
				 	$providerInfoFiles = $this->files->files($providers);
					if (!$providerInfoFiles) return;

					$providerFiles = [];
					if ($providerInfoFiles[0] instanceof \Symfony\Component\Finder\SplFileInfo){ // support 5.8
						$providerFiles = array_map(function($val){
							return $val->getFilename();
						},$this->files->files($providers));
					}
					else{
						$providerFiles = array_map("class_basename",$this->files->files($providers));
					}
					
					foreach ($providerFiles as $provider)
					{
						$this->app->register('\\Quatius\\'.$module.'\\Providers\\'.substr($provider, 0, -4));
					}
				}
				
			}
		}
		
		$this->app->register('\\Prettus\\Repository\\Providers\\RepositoryServiceProvider');
	}
	
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['quatius'];
	}
}
<?php

namespace Quatius\Framework\Traits;

use Illuminate\Database\Eloquent\Model;
use DB;
use Event;
/**
 * @author seyla.keth
 *
 */

trait MorphLinkage
{
    
    /**
     * 
     * 
     * @return array [morph=>'tag_assign', link_id=>'tag_id', table(optional), values(optional)]
     */
    abstract function getLinkFields();
    
    protected function _getLinkFields(){
        $linkFields = $this->getLinkFields();
        if (!isset($linkFields['morph'])){
            throw new \Exception("Link Field missing morph name");
        }
        if (!isset($linkFields['link_id'])){
            throw new \Exception("Link Field missing link_id");
        }
        
        $linkFields['table'] = isset($linkFields['table'])?$linkFields['table']:$linkFields['morph'].'s';
        $linkFields['morph_type'] = isset($linkFields['morph_type'])?$linkFields['morph_type']:$linkFields['morph'].'_type';
        $linkFields['morph_id'] = isset($linkFields['morph_id'])?$linkFields['morph_id']:$linkFields['morph'].'_id';
        
        return $linkFields;
    }
    
    public function deleteLinkFrom($relationClass){
        $classPath = "";
        $classId = $id;
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            $classId = $relationClass->id;
        }
        if (!$classId || $classPath == ""){
            return;
        }
        
        $linkFields = $this->_getLinkFields();
        
        $result = DB::table($linkFields['table'])->where($linkFields['morph_type'], $classPath)->where($linkFields['morph_id'], $classId)->delete();
        
        $this->clearCache('delete-link');
        
        eventFire("quatius.update-link",[$linkFields['morph'],'delete', $classPath, $classId,[],$result, $this]);
    }
    
    public function updateValues($linkDatas, $relationClass, $id = null, $detach = false, $removeDuplicate = false){
        
        $classPath = "";
        $classId = $id;
        
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            $classId = $relationClass->id;
        }
        
        if (!$classId){
            return;
        }
        
        $linkFields = $this->_getLinkFields();
        
        $linkQuery = DB::table($linkFields['table']);
        
        if ($detach){
            $linkQuery->where($linkFields['morph_type'], $classPath)->where($linkFields['morph_id'], $classId)->delete();
        }
        
        $datas = [];
        $results = [];
        foreach ($linkDatas as $data){
            $data[$linkFields['morph_id']] = intval($classId);
            $data[$linkFields['morph_type']] = $classPath;
            
            if (isset($linkFields['values']) && $linkFields['values']){
                $data = array_merge($linkFields['values'], $data);
            }
            
            if (!isset($data[$linkFields['link_id']])){
                throw new \Exception("Link id required");
            }
            
            if (!$detach && $removeDuplicate){
                $linkQuery->where($linkFields['morph_type'], $classPath)
                ->where($linkFields['morph_id'], $classId)
                ->where($linkFields['link_id'], $data[$linkFields['link_id']])
                ->delete();
            }
            
            $results[] = $linkQuery->insert($data);
            
            $datas[] = $data;
        }
        $this->clearCache('update-link');
        
        eventFire("quatius.update-link",[$linkFields['morph'],'insert', $classPath, $classId, $datas, $results, $this]);
    }
    
    public function removeByValue($values=[], $relationClass, $id=null)
    {
        $classPath = "";
        $classId = $id;
        
        if ($relationClass){ // this will 
            if (is_string($relationClass)){
                $classPath = $relationClass;
            }
            elseif ($relationClass instanceof Model)
            {
                $classPath = get_class($relationClass);
                $classId = $relationClass->id;
            }
            
            if (!$classId){
                return;
            }
        }elseif(!$values){ // must not be null to avoid deleting all the data in the table.
            return;
        }
        
        $linkFields = $this->_getLinkFields();
        
        $linkQuery = DB::table($linkFields['table']);
        
        if ($relationClass){
            $linkQuery->where($linkFields['morph_type'], $classPath)->where($linkFields['morph_id'], $classId);
        }
        
        $isSet = false;
        foreach ($values as $key=>$value){
            $isSet = true;
            if (is_array($value)){
                $linkQuery->whereIn($key, $value);
            }
            else{
                $linkQuery->where($key, $value);
            }
        }
        
        if ($isSet){
            $result = $linkQuery->delete();
            $this->clearCache('remove-by-value');
            eventFire("quatius.update-link",[$linkFields['morph'],'remove-by-value', $classPath, $classId,$values,$result, $this]);
        }
        
        return $result;
    }
    
    public function getLinksFrom($relationClass, $id=null, $byValues=[], $associateClass=true)
    {
        if (!$this->allowedCache('getLinksFrom') || $this->isSkippedCache()) {
            return $this->_getLinksFrom($relationClass, $id, $byValues, $associateClass);
        }
        
        $key = $this->getCacheKey('getLinksFrom', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($relationClass, $id, $byValues, $associateClass) {
            return $this->_getLinksFrom($relationClass, $id, $byValues, $associateClass);
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    protected function _getLinksFrom($relationClass, $id=null, $byValues=[], $associateClass=true)
    {
        if (!method_exists($this, 'makeModel')){
            return [];
        }
        $classPath = "";
        $classId = $id;
        if (is_string($relationClass)){
            $classPath = $relationClass;
        }
        elseif ($relationClass instanceof Model)
        {
            $classPath = get_class($relationClass);
            if (!is_array($classId)){
                $classId = $relationClass->id;
            }
        }
        $linkFields = $this->_getLinkFields();
        
        $strValues = "";
        
        if (isset($linkFields['values']) && $linkFields['values']){
            $strValues = implode('`, `', array_keys($linkFields['values']));
        }
        
        if (!!($strValues)){
            $strValues = "`$strValues`, ";
        }
        
        $rawJoin = "SELECT $strValues `{$linkFields['link_id']}` FROM `{$linkFields['table']}`"
        ." WHERE `{$linkFields['morph_type']}` = '".addslashes($classPath)."'";
        
        if ($classId){
            if (is_array($classId)){
                $rawJoin .=" AND `{$linkFields['morph_id']}` in ('".implode($glue, $classId)."')";
            }
            else{
                $rawJoin .=" AND `{$linkFields['morph_id']}` = '$classId'";
            }
        }
        
        if ($byValues){
            foreach ($byValues as $key=>$value){
                $value = addslashes($value);
                $rawJoin .=" AND `$key` = '$value'";
            }
        }
        
        if(!$associateClass){
            return collect(DB::select($rawJoin));
        }
        
        $linkQuery = $this->makeModel()->query();
        $linkQuery->join(DB::raw("(".$rawJoin.") AS morphlink"), "{$linkFields['link_id']}", '=', 'id');
        
        return $linkQuery->get();
    }
}

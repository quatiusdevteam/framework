<?php

namespace Quatius\Framework\Traits;

/**
 * @author seyla.keth
 *
 */

trait ModelFinder
{
    public function findOrNew($id, $attributes = []){
        $model = parent::find($id);
        if (!$model){
            $model = $this->newInstance($attributes);
        }
        return $model;
    }
    
    public function findOrCreate($id, $attributes = []){
        $model = parent::find($id);
        if (!$model){
            $model = $this->newInstance($attributes);
            $model->save();
        }
        return $model;
    }
    
    public function findOrFail($id){
        $model = parent::find($id);
        abort_unless($model, 404);
        return $model;
    }
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKey()
    {
        return hashids_encode($this->getKey());
    }
}

<?php

/**
 * @author seyla.keth
 *
 */
namespace Quatius\Framework\Traits;

use Event;


trait CacheableUpdate
{   

    /**
     * Get cache minutes
     *
     * @return int
     */
    public function getCacheMinutes()
    {
        $cacheMinutes = isset($this->cacheMinutes) ? $this->cacheMinutes : config('repository.cache.minutes', 30);

        /**
         * https://laravel.com/docs/5.8/upgrade#cache-ttl-in-seconds
         */
        if (5.8 <= floatval(app()->version()))
            return $cacheMinutes * 60;

        return $cacheMinutes;
    }

	public function cache($key, $func, $funcArgs=[],  $minutes=0)
    {
        if (!$key) throw new \Exception("key must not be empty");
        if (!is_callable($func)) throw new \Exception("\$func is not callable");
        
        $keyArgs = is_array($funcArgs)?$funcArgs:[$funcArgs];
        
        if ($this->isSkippedCache()) {
            return call_user_func_array($func, $keyArgs);
        }
        
        $key = $this->getCacheKey('cache.'.$key, $keyArgs);
        $minutes = $minutes > 0?$minutes:$this->getCacheMinutes();

        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($func, $keyArgs) {
            return call_user_func_array($func, $keyArgs);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    public function findWhereIn($field, array $values, $columns = ['*'])
    {
        if (!$this->allowedCache('findWhereIn') || $this->isSkippedCache()) {
            return parent::findWhereIn($field, $values, $columns);
        }

        $key = $this->getCacheKey('findWhereIn', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($field, $values, $columns) {
            return parent::findWhereIn($field, $values, $columns);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }
	
    public function clearFunctionCache($funcName, $args=[]){
        $key = $this->getCacheKey($funcName, $args);
        return $this->getCacheRepository()->forget($key);
    }
    /**
     * @param string $callByAction
     */
    public function clearCache($callByAction=""){
        $group = get_called_class();
        $keys = $this->loadKeys();
        if (isset($keys[$group])){
            foreach ($keys[$group] as $key){
                $this->getCacheRepository()->forget($key);
            }
            unset($keys[$group]);
        }
        
        if ($this instanceof \Prettus\Repository\Eloquent\BaseRepository){
            $file = \Prettus\Repository\Helpers\CacheKeys::getFileKeys();
        }
        else {
            $file = \Litepie\Repository\Helpers\CacheKeys::getFileKeys();
        }
        
        $content  = json_encode($keys);
        
        file_put_contents($file, $content);
        
        eventFire('quatius.cache-clear',[$group, $callByAction, $keys, $this]);
    }
    
    /**
     * @return array|mixed
     */
    private function loadKeys()
    {
        if ($this instanceof \Prettus\Repository\Eloquent\BaseRepository){
            $file = \Prettus\Repository\Helpers\CacheKeys::getFileKeys();
        }
        else {
            $file = \Litepie\Repository\Helpers\CacheKeys::getFileKeys();
        }
        
        if (!file_exists($file)) {
            return [];
        }
        
        $content = file_get_contents($file);
        $keys = json_decode($content, true);
        
        return $keys;
    }
    
    public function create(array $attributes)
    {
        if (!$this->isSkippedCache() && config('database.cache.clean.enabled', true) && config('database.cache.clean.on.create', true) ){
            $this->clearCache('create');
        }
        
        return parent::create($attributes);
    }
    
    public function update(array $attributes, $id)
    {
        if (!$this->isSkippedCache() && config('database.cache.clean.enabled', true) && config('database.cache.clean.on.update', true) ){
            $this->clearCache('update');
        }
        
        return parent::update($attributes, $id);
    }
    
    public function delete($id)
    {
        if (!$this->isSkippedCache() && config('database.cache.clean.enabled', true) && config('database.cache.clean.on.delete', true) ){
            $this->clearCache('delete');
        }
        
        return parent::delete($id);
    }
}

/**
 *
 * copyright [year] [your Business Name and/or Your Name].
 * email: your@email.com
 * license: Your chosen license, or link to a license file.
 *
 */
(function (factory) {
        /* Global define */
        if (typeof define === 'function' && define.amd) {
            // AMD. Register as an anonymous module.
            define(['jquery'], factory);
        } else if (typeof module === 'object' && module.exports) {
            // Node/CommonJS
            module.exports = factory(require('jquery'));
        } else {
            // Browser globals
            factory(window.jQuery);
        }
    }

    (function ($) {

        /**
         * @class plugin.examplePlugin
         *
         * example Plugin
         */
        $.extend(true, $.summernote.lang, {
            'en-US': {
                bookmark: {
                    exampleText: '',
                    dialogTitle: 'Set Bookmark',
                    okButton: 'OK'
                }
            }
        });

        $.extend($.summernote.options, {
            bookmark: {
                icon: '<i class="fa fa-bookmark"/>',
                tooltip: 'Set bookmark title',
            }
        });

        $.extend($.summernote.plugins, {
            /**
             *  @param {Object} context - context object has status of editor.
             */
            'bookmark': function (context) {
                var self      = this,
                    ui        = $.summernote.ui,
                    $editor   = context.layoutInfo.editor,
                    $editable = context.layoutInfo.editable,
                    $note     = context.layoutInfo.note,
                    options   = context.options,
                    lang      = options.langInfo;

                context.memo('button.bookmark', function() {
                    var button = ui.button({
                        contents: options.bookmark.icon,
                        tooltip:  lang.bookmark.tooltip,
                        click:    function (e) {
                            //var range = context.invoke("editor.createRange").getWordRange();
                            var text = context.invoke('editor.createRange').toString();
                            if(text.trim() != ""){
                                self.$dialog.find("#bookmark_name").val(text.trim().replace(/ /g, "-"));
                                self.$dialog.find("#summernote-bookmark-sample").html(text.trim().replace(/ /g, "-"));
                                self.$dialog.find("#selected_text").val(text);

                                context.invoke('bookmark.show');
                            }
                            //
                        }
                    });
                    return button.render();
                });

                this.initialize = function () {
                    var $container = options.dialogsInBody ? $(document.body) : $editor;
                    // Build the Body HTML of the Dialog.
                    var body = '<div class="input-group" id="summernote-bookmark-dialog">' +
                        '<label class="input-group-addon">Bookmark name:</label> ' +
                        '<input type="text" class="form-control" name="bookmark_name" id="bookmark_name" onkeyup="transferText();">' +
                        '<input type="hidden" name="selected_text" id="selected_text">' +
                        '</div>' +
                        '<div class="input-group">' +
                        '<span style="font-size: 14px; padding: 15px;">Bookmark text: </span> ' +
                        '<span style="font-size: 14px; font-weight: bold" id="summernote-bookmark-sample"></span>' +
                        '</div> ';
                    var footer = '<button href="#" class="btn btn-primary bookmark-ok-btn">' + lang.bookmark.okButton + '</button>'

                    this.$dialog = ui.dialog({
                        title: lang.bookmark.dialogTitle,
                        body: body,
                        footer: footer
                    }).render().appendTo($container);
                };

                this.destroy = function () {
                    ui.hideDialog(this.$dialog);
                    this.$dialog.remove();
                };

                this.bindEnterKey = function ($input, $btn) {
                    $input.on('keypress', function (event) {
                        if (event.keyCode === 13) $btn.trigger('click');
                    });
                };

                this.bindLabels = function () {
                    self.$dialog.find('.form-control:first').focus().select();
                    self.$dialog.find('label').on('click', function () {
                        $(this).parent().find('.form-control:first').focus();
                    });
                };

                this.show = function () {
                    var $img = $($editable.data('target'));
                    var editorInfo = {

                    };
                    this.showBookmarkPluginDialog(editorInfo).then(function (editorInfo) {
                        var bookmarkText = self.$dialog.find("#bookmark_name").val();

                        if(bookmarkText.trim() != ""){
                            var selectedText = self.$dialog.find("#selected_text").val();
                            var node = $("<span>" + selectedText+ "</span>");
                                node.attr('id', bookmarkText.trim().replace(/ /g, "-"));

                            context.invoke('restoreRange');
                            context.invoke('editor.insertNode', node[0]);
                        }else{
                            context.invoke('editor.restoreRange');
                        }

                        ui.hideDialog(self.$dialog);

                    });
                };
                this.showBookmarkPluginDialog = function(editorInfo) {
                    return $.Deferred(function (deferred) {
                        ui.onDialogShown(self.$dialog, function () {
                            context.triggerEvent('dialog.shown');
                            $editBtn = self.$dialog.find('.bookmark-ok-btn');
                            $editBtn.click(function (e) {
                                e.preventDefault();
                                deferred.resolve({

                                });
                            });
                            self.bindEnterKey($editBtn);
                            self.bindLabels();
                        });
                        ui.onDialogHidden(self.$dialog, function () {
                            $editBtn.off('click');
                            if (deferred.state() === 'pending') deferred.reject();
                        });
                        ui.showDialog(self.$dialog);
                    });
                };
            }
        });
    })
);

function transferText() {
    var bookmark_name =  $("#summernote-bookmark-dialog").find("#bookmark_name").val();
    $("#summernote-bookmark-sample").html(bookmark_name.trim().replace(/ /g, "-"));

}
$(document).ready(function () {
    callme(1);
});
$( document ).ajaxComplete(function() {
    callme(2);

});
function callme(who){
    $('.html-editor').on("summernote.init", function () {
        console.log(who);
    }) ;

}
<?php

namespace Quatius\Framework\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Cache;

class Save extends Model
{
    protected $fillable =['user_id'];
    
    public static function countSaveModel($strClassPath)
    {
    	return count(self::getSaveModelId($strClassPath));
    } 
    
    public static function clearCache($strClassPath)
    {
    	$user_id = Auth::id();
    	Cache::forget("user_{$user_id}_saved_{$strClassPath}");
    }
    
    public static function getSaveModelId($strClassPath)
    {
    	$user_id = Auth::id();
    	return Cache::remember("user_{$user_id}_saved_{$strClassPath}", 60, function() use ($user_id, $strClassPath) {
    		
    		return Save::whereUser_id($user_id)->whereSaveable_type($strClassPath)->pluck('saveable_id')->toArray();
    	});
    }
}

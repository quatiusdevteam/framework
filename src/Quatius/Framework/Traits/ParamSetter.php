<?php

namespace Quatius\Framework\Traits;

trait ParamSetter
{	
    private $_paramSetter = [];

    private function decodeJson($jsonTxt, $default=[]){
        $data = json_decode($jsonTxt, true);
        
        return $data?:$default;
    }
    
    private function encodeJson($jsonObj){
        return json_encode($jsonObj);
    }
    
    private function getDefaultParams(){
        return 'params';
    }
    
    public function getGetParamsAttribute(){
        return $this->getParams();
    }
    
    public function getParamsOnly($keys = []){
        return array_intersect_key($this->getParams(), array_flip($keys));
    }
    
    public function getParams($field = null, $default=null, $savedParamName = null){
        $savedParamName = $savedParamName?:$this->getDefaultParams();

        if (!isset($this->$savedParamName) || (!$this->$savedParamName)) $this->$savedParamName = '{}';
		
        if (!isset($this->_paramSetter[$savedParamName])){
            $this->_paramSetter[$savedParamName] = $this->decodeJson($this->$savedParamName);
        };
        
        if (!$field){
            return $this->_paramSetter[$savedParamName];
        }
        elseif(array_has($this->_paramSetter[$savedParamName],$field)){
            return array_get( $this->_paramSetter[$savedParamName], $field);
        }
        return $default;
    }

    public function hasParams($field, $savedParamName = null){
        $savedParamName = $savedParamName?:$this->getDefaultParams();
        
        if (!isset($this->$savedParamName) || (!$this->$savedParamName)) $this->$savedParamName = '{}';
		
        if (!isset($this->_paramSetter[$savedParamName])){
            $this->_paramSetter[$savedParamName] = $this->decodeJson($this->$savedParamName);
        };
        
        if (!$field){
            return false;
        }
        elseif(array_has($this->_paramSetter[$savedParamName],$field)){
            return true;
        }
        return false;
    }
    
    public function setParams($field, $value, $savedParamName = null){
        $savedParamName = $savedParamName?:$this->getDefaultParams();
        
        if (!isset($this->$savedParamName) || (!$this->$savedParamName)) $this->$savedParamName = '{}';
        
        if (!isset($this->_paramSetter[$savedParamName])){
            $this->_paramSetter[$savedParamName] = $this->decodeJson($this->$savedParamName);
        };
        
        if(!is_scalar($field)) throw new \Exception("$field is null/object in class(".get_class().")");
            
        array_set( $this->_paramSetter[$savedParamName], $field, $value);
        
        $this->$savedParamName = $this->encodeJson($this->_paramSetter[$savedParamName]);
    }
        
    public function pushParams($field, $value, $savedParamName = null){
        $tmpArray = $this->getParams($field,[]);
        array_push($tmpArray, $value);
        $this->setParams($field, $tmpArray);
    }

    public function mergeParams($field, $value, $savedParamName = null){
        $this->setParams($field, array_merge($this->getParams($field,[]), $value));
    }
	
    public function unsetParams($field, $savedParamName = null){
        $savedParamName = $savedParamName?:$this->getDefaultParams();
        
        if (!isset($this->$savedParamName) || (!$this->$savedParamName)) $this->$savedParamName = '{}';
        
        if (!isset($this->_paramSetter[$savedParamName])){
            $this->_paramSetter[$savedParamName] = $this->decodeJson($this->$savedParamName);
        };
        
        if(!is_scalar($field)) return ;
        
        array_forget($this->_paramSetter[$savedParamName], $field);
        
        $this->$savedParamName = $this->encodeJson($this->_paramSetter[$savedParamName]);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelItemGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('model_item_groups', function (Blueprint $table) {
            $table->string('group_name',200);
            $table->string('grouptable_type');
            $table->string('group_data',5000);
            $table->integer('grouptable_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('model_item_groups');
    }
}

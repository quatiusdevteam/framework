<?php

return [

    'disable'=>['JoomlaHasher'
	],

	'cache'=>[
		'clean-interval'=> 14400,
		'url-filter-enabled'=> true
	],

];

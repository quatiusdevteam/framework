<?php

namespace Quatius\Framework\Traits;
use Quatius\Framework\Models\Save;

use Auth;

trait Saveability
{
	function getSavesCountAttribute()
    {
    	return $this->saves()->count();
    }
    
    public static function countSaveModel()
    {
    	return count(Save::getSaveModelId(get_called_class()));
    }
    
    public function toggle()
    {
    	$this->isSaved() ? $this->unsave() : $this->save();
    }
    
    public function getClass(){
    	return get_class($this);
    } 
    
    public function isSaved()
    {
    	return array_search("{$this->id}",Save::getSaveModelId(get_class($this))) !== false;
    }
    
    public function saves()
    {
    	return $this->morphMany(Save::class,'saveable');
    }
    
    public function saveForLater()
    {
    	if ($this->isSaved())
    		return;
    	
    	
    	$save = new Save(['user_id'=>Auth::id()]);
    	
    	$this->saves()->save($save);
    	Save::clearCache(get_class($this));
    }
    
    public function removeSaved()
    {
		$this->saves()
			->where('user_id', Auth::id())
			->delete();
		Save::clearCache(get_class($this));
    }
}

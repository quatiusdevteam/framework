<?php

namespace Quatius\Framework\Models;

use Illuminate\Database\Eloquent\Model;

class ModelItemGroup extends Model
{
	protected $fillable = [
		'group_name',
		'group_data',
	];
	
	public function grouptable()
    {
    	return $this->morphTo();
    }
}

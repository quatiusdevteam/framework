<?php

namespace Quatius\Framework\Models;

class CsvImporter
{
	private $fp;
	private $parse_header;
	private $header;
	private $delimiter;
	private $length;
	public $total=0;
	//--------------------------------------------------------------------
	function __construct($file_name, $parse_header=false, $delimiter=",", $length=8000)
	{
	    $this->total = self::getLines($file_name);
		$this->fp = fopen($file_name, "r");
		$this->parse_header = $parse_header;
		$this->delimiter = $delimiter;
		$this->length = $length;
		if ($this->parse_header)
		{
			$this->header = fgetcsv($this->fp, $this->length, $this->delimiter);
			foreach ($this->header as $i => $heading_i)
				$this->header[$i] = trim($heading_i);
		}

	}
	//--------------------------------------------------------------------
	function __destruct()
	{
		if ($this->fp)
		{
			fclose($this->fp);
		}
	}
	
	static function getLines($file)
	{
	    $f = fopen($file, 'rb');
	    $lines = 0;
	    
	    while (!feof($f)) {
	        $lines += substr_count(fread($f, 8192), "\n");
	    }
	    
	    fclose($f);
	    
	    return $lines;
	}
	
	//--------------------------------------------------------------------
	function get($max_lines=0, $utf=false)
	{
		//if $max_lines is set to 0, then get all the data

		$data = array();

		if ($max_lines > 0)
			$line_count = 0;
		else
			$line_count = -1; // so loop limit is ignored

		while ($line_count < $max_lines && ($row = fgetcsv($this->fp, $this->length, $this->delimiter)) !== FALSE)
		{
			if ($this->parse_header)
			{
				foreach ($this->header as $i => $heading_i)
				{
				    
				    $row_new[$heading_i] = $utf?utf8_encode($row[$i]):$row[$i];
				}
				$data[] = $row_new;
			}
			else
			{
				$data[] = $row;
			}

			if ($max_lines > 0)
				$line_count++;
		}
		return $data;
	}
	//--------------------------------------------------------------------

}
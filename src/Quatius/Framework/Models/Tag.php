<?php

namespace Quatius\Frameworkn\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Quatius\Media\Traits\MediaLink;

class Tag extends Model
{
	use MediaLink;
	
    protected $fillable = ['name','description','status','group','publish_start','publish_end'];

    protected $dates = ['publish_start','publish_end'];
    
    protected $primaryKey = 'id';
    
	public function scopeSearch($query, $search){
		$search = trim($search);
		if(!empty($search)){
			$query->where('name','like', "%".trim($search)."%")->orWhere('description','like',"%".trim($search)."%");
		}
		return $query;
	}
	
	public function scopePublished($query){
		$query->whereStatus(2);
		
		return $query;
	}
	
	public function getCreatedAtAttribute($date){
		$date = $date ?: date('Y-m-d H:i:s');
		return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
	}
	public function getUpdatedAtAttribute($date){
		if(empty($date)) return null;
		return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
	}
	public function getPublishStartAttribute($date){
		if(empty($date)) return null;
		return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
	}
	public function getPublishEndAttribute($date){
		if(empty($date)) return null;
		return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
	}
	
	public function setPublishStartAttribute($date){
		$this->attributes['publish_start'] = $this->storageFormat($date);
	}
	
	public function setPublishEndAttribute($date){
		$this->attributes['publish_end'] = $this->storageFormat($date);
	}
	
	protected function storageFormat($date){
		if(empty($date)) return null;
		return Carbon::createFromFormat('d-m-Y H:i:s',$date)->format('Y-m-d H:i:s');
	}
}

<?php

namespace Quatius\Framework\Traits;
use Quatius\Framework\Models\Tag;

trait Tagging
{
	public function tag(){
		return $this->morphToMany(Tag::class, 'tag_model')->withPivot(['value','status']);
	}
	
	public function hasTag($value=null, $group = null){
		$tag = $this->getTag($value,$group);
		return !!$tag;
	}
	
	public function getDefaultTag($group = ""){
		return $this->tag()->whereGroup($group)->wherePivot('value','=' ,"_default")->first();
	}
	
	public function setDefaultTag($tag){
		$defaultValue = "_default";
		$otherTagIds = $this->tag()->whereGroup($tag->group)->wherePivot('value','=' ,$defaultValue)->pluck('id');
		
		foreach ($otherTagIds as $delId)
			$this->removeTag($delId, $defaultValue);
		
		$this->setTag($tag->id,$defaultValue,2,$tag->group);
		return null;
	}
	
	public function getTag($value="", $group = null){
		$tags = $this->getTags($value, $group);
		
		if ($tags->count() > 0)
		{
			foreach ($tags as $tag)
				if ($tag->status == 2)
					return $tag;
		}
		
		return null;
	}
	
	public function setSingleTag($tag, $value = "", $status=2){
		if (is_int($tag) || is_string($tag))
			$tag = Tag::find($tag);
		
		$otherTagIds = $this->tag()->whereGroup($tag->group)->wherePivot('value','=' ,$value)->pluck('id');
		
		foreach ($otherTagIds as $delId)
			$this->removeTag($delId, $value);
		
		$tagDefault = $this->getDefaultTag($tag->group);
		
		if ($tagDefault)
		{
			if ($tagDefault->id == $tag->id && $status == 2)
				return; // use default;
		}
		
		$this->setTag($tag->id, $value, $status);
	}
	
	public function setTag($tag, $value = "", $status=2){
		if (is_int($tag))
			$tag_id = $tag;
		else 
			$tag_id = $tag->id;
		
		$savedTag = $this->tag()->whereId($tag_id)->wherePivot('value','=', $value)->first();
		 
		if ($savedTag)
		{
			$savedTag->pivot->status=$status;
			$savedTag->pivot->save();
			
		}
		else 
		{
			$tag = Tag::find($tag_id);
			if ($tag)
				$this->tag()->save($tag, ["value"=>$value, "status"=>$status]);
		}
	}
	
	public function removeTag($tag_id, $value = ""){
		$this->tag()->newPivotStatementForId($tag_id)->where('value','=', $value)->delete();
	}
	
	public function getTags($value="", $group=null){
		$defaultValue = "_default";
		
		if ($group)
		{
			$tags = $this->tag()->whereGroup($group)->wherePivot('value','=',$value)->groupby('tag_id')->distinct()->get();
			if ($tags->count() > 0)
				return $this->tag()->whereGroup($group)->wherePivot('status','=',2)->wherePivot('value','=',$value)->groupby('tag_id')->distinct()->get();
			else
				return $this->tag()->whereGroup($group)->wherePivot('status','=',2)->wherePivot('value','=', $defaultValue)->groupby('tag_id')->distinct()->get();
		}
		else
		{
			$tags = $this->tag()->wherePivot('value','=',$value)->groupby('tag_id')->distinct()->get();
			if ($tags->count() > 0)
				return $this->tag()->wherePivot('status','=',2)->wherePivot('value','=',$value)->groupby('tag_id')->distinct()->get();
			else 
				return $this->tag()->wherePivot('status','=',2)->wherePivot('value','=', $defaultValue)->groupby('tag_id')->distinct()->get();
		}
	}
}
